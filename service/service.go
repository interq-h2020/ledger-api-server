package service

import (
	"github.com/hyperledger/fabric-sdk-go/pkg/client/ledger"
	"github.com/hyperledger/fabric-sdk-go/pkg/gateway"
)

//FabricClient The Hyperledger Fabric client where all operations are defined
type FabricClient struct {
	FabricContract *gateway.Contract

	//New one added
	LedgerClient *ledger.Client
}
