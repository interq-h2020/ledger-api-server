package service

import (
	"fmt"
	"log"

	"gitlab.com/inlecom/InterQ/ledger-api-server/go-gen/models"
)


func (c *FabricClient) CheckQhd(subject string, model string, timeref string) (string,error) {
	log.Println("==========Timeref Check Start=============")
	results, err := c.FabricContract.EvaluateTransaction("CheckKey", subject, model, timeref)
	if err != nil {
		fmt.Println("error in CheckTime : ", err)
		return "",err
	}
	if len(results) > 0 {
		return "409", nil
	}
	log.Println("==========Timeref Check End=============")
	return "", nil
}


func (c *FabricClient) PublishQhds(qhdHeader *models.QhdQhdheader, uID string, hash string, cID string, pwd string, scope string, hashAlg string) ([]byte, error) {

	log.Println("==========PublishQhd-starts=============")
	//Get a string representation of qhd.Body
	//qhdBody := fmt.Sprintf("%v", qhd.Body["Body"])
	//fmt.Println("qhdBody : ", qhdBody)

	results, err := c.FabricContract.SubmitTransaction("PublishQhd",
		uID, qhdHeader.Owner, qhdHeader.Asset, qhdHeader.Timeref, qhdHeader.Model, qhdHeader.Subject, hash, cID, hashAlg, scope)
	if err != nil {
		return nil, err
	}
	fmt.Println("Result is : ", string(results))

	log.Println("==========PublishQhd-ends=============")

	return results, nil

}

func (c *FabricClient) SearchByCriteria(subj string, model string, asset string, owner string, timerefFrom string, timerefTill string) ([]byte, error) {
	log.Println("==========SearchByCriteria-starts=============")
	result, err := c.FabricContract.EvaluateTransaction("ReadQhdsByCriteria", subj, model, asset, owner, timerefFrom, timerefTill)
	if err != nil {
		fmt.Println("err in ReadQhd : ", err)
		return nil, err
	}

	log.Println("==========SearchQhdByCriteria-starts=============")
	return result, nil
}

func (c *FabricClient) SearchQhdByID(uuid string, pwd string, cID string) ([]byte, error) {
	log.Println("==========SearchQhdByID-starts=============")
	result, err := c.FabricContract.EvaluateTransaction("ReadQhd", uuid)
	if err != nil {
		fmt.Println("err in ReadQhd : ", err)
		return nil, err
	}

	log.Println("Result is : ", string(result))

	log.Println("==========SearchQhdByID-ends=============")

	return result, nil
}

func (c *FabricClient) SearchQHDsByAssetSerialNumber(assetSerialNumber string) ([]byte, error) {

	log.Println("==========SearchQHDsByAssetSerialNumber-starts=============")
	result, err := c.FabricContract.EvaluateTransaction("ReadQhdsByAssetSerialNumber", assetSerialNumber)
	if err != nil {
		fmt.Println("err in SearchQhdByAssetSerialNumber : ", err)
		return nil, err
	}
	//return result, nil
	log.Println("==========SearchQHDSByAssetSerialNumber-ends=============")
	return result, nil
}

func (c *FabricClient) SearchQHDsByOwnerID(owner string) ([]byte, error) {

	log.Println("==========SearchQHDsByOwnerID-starts=============")
	result, err := c.FabricContract.EvaluateTransaction("ReadQhdsByOwnerID", owner)
	if err != nil {
		fmt.Println("err in SearchQhdByAssetSerialNumber : ", err)
		return nil, err
	}
	//return result, nil
	log.Println("==========SearchQHDSByOwnerID-ends=============")
	return result, nil
}
