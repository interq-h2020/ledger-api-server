/*
Interaction with chaincode

Wallet setup
*/

package service

import (
	"fmt"
	"io/ioutil"
	"log"
	"path/filepath"

	"github.com/hyperledger/fabric-sdk-go/pkg/client/ledger"

	"gitlab.com/inlecom/InterQ/ledger-api-server/configs"

	"github.com/hyperledger/fabric-sdk-go/pkg/core/config"
	"github.com/hyperledger/fabric-sdk-go/pkg/gateway"
)

// GetFabricContract description
func GetFabricContract() (*FabricClient, error) {
	envcfg, err := configs.InitConfig()
	if err != nil {
		log.Fatal(err)
	}
	log.Println("ENV: ", envcfg)

	// err = os.Setenv("DISCOVERY_AS_LOCALHOST", "false")
	// if err != nil {
	// 	log.Fatalf("Error setting DISCOVERY_AS_LOCALHOST environemnt variable: %v", err)
	// }

	wallet, err := gateway.NewFileSystemWallet("wallet")
	if err != nil {
		log.Fatalf("Failed to create wallet: %v", err)
	}

	if !wallet.Exists("appUser") {
		err = populateWallet(wallet, envcfg.NET_PATH)
		if err != nil {
			log.Fatalf("Failed to populate wallet contents: %v", err)
		}
	}

	ccpPath := filepath.Join(
		envcfg.NET_PATH,
		"organizations",
		"peerOrganizations",
		"org1.example.com",
		"connection-org1.yaml",
	)

	gw, err := gateway.Connect(
		gateway.WithConfig(config.FromFile(filepath.Clean(ccpPath))),
		gateway.WithIdentity(wallet, "appUser"),
	)
	if err != nil {
		log.Fatalf("Failed to connect to gateway: %v", err)
	}
	defer gw.Close()

	network, err := gw.GetNetwork(envcfg.CHANNEL)
	if err != nil {
		log.Fatalf("Failed to get network: %v", err)
	}

	c := network.GetContract(envcfg.CONTRACT)

	/*
		sdk, err := fabsdk.New(config.FromFile(filepath.Clean(ccpPath)))
		if err != nil {
			log.Fatalf("Failed to create sdk : %v", err)
		}
		context := sdk.ChannelContext("channel0")
		if err != nil {
			fmt.Println("Failed to create ctx")
		}
		client ,err := ledger.New(context)
		if err != nil {
			fmt.Println("Failed to create client")
		}

		if client != nil {
			fmt.Println("client created : ", &ledger.Client{})
		}

	*/

	return &FabricClient{FabricContract: c, LedgerClient: &ledger.Client{}}, nil
}

func populateWallet(wallet *gateway.Wallet, netpath string) error {
	log.Println("============ Populating wallet ============")
	credPath := filepath.Join(
		netpath,
		"organizations",
		"peerOrganizations",
		"org1.example.com",
		"users",
		"User1@org1.example.com",
		"msp",
	)

	//certPath := filepath.Join(credPath, "signcerts", "User1@org1.example.com-cert.pem")
	certPath := filepath.Join(credPath, "signcerts", "cert.pem")
	// read the certificate pem
	cert, err := ioutil.ReadFile(filepath.Clean(certPath))
	if err != nil {
		return err
	}

	keyDir := filepath.Join(credPath, "keystore")
	// there's a single file in this dir containing the private key
	files, err := ioutil.ReadDir(keyDir)
	if err != nil {
		return err
	}
	if len(files) != 1 {
		return fmt.Errorf("keystore folder should have contain one file")
	}
	keyPath := filepath.Join(keyDir, files[0].Name())
	key, err := ioutil.ReadFile(filepath.Clean(keyPath))
	if err != nil {
		return err
	}

	identity := gateway.NewX509Identity("Org1MSP", string(cert), string(key))

	return wallet.Put("appUser", identity)
}
