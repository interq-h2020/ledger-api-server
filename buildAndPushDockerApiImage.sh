#!/bin/bash

docker build -t registry.gitlab.com/inlecom/interq/ledger-api-server .
docker login registry.gitlab.com -u ${USR} -p ${PSWD}
docker push registry.gitlab.com/inlecom/interq/ledger-api-server
