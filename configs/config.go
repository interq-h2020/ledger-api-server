package configs

import (
	"fmt"
	"log"
	"os"

	"github.com/caarlos0/env"
	"github.com/joho/godotenv"
)

type EnvConfig struct {
	CHANNEL                string `env:"CHANNEL,required"`
	CONTRACT               string `env:"CONTRACT,required"`
	NET_PATH               string `env:"NET_PATH,required"`
	WORKDIR                string `env:"WORKDIR,required"`
	DISCOVERY_AS_LOCALHOST string `env:"DISCOVERY_AS_LOCALHOST" envDefault:"false"`
}

type CertConfig struct {
	CERT_PATH 	       string `env:"CERT_PATH,required"`	
}


func loadDotEnv() error {
	err := godotenv.Load()
	if err != nil {
		return err
	}

	return nil
}

/*
func InitCertConfig() (*CertConfig, error) {
	cfg := CertConfig{}
	if err := env.Parse(&CertConfig); err != nil {
		return nil, err
	}

}
*/

func InitConfig() (*EnvConfig, error) {

	// This command will return an error when deployed as a container, because a .env file does not exist
	err := loadDotEnv()
	if err != nil {
		log.Printf("Error loading .env file : %s", err)
	}

	// This command will run in a container, as the values are passed as envirnoment via the docker-compose.yaml file.
	cfg := EnvConfig{}
	if err := env.Parse(&cfg); err != nil {
		return nil, err
	}

	err = os.Setenv("DISCOVERY_AS_LOCALHOST", cfg.DISCOVERY_AS_LOCALHOST)
	if err != nil {
		log.Fatalf("Error setting DISCOVERY_AS_LOCALHOST environment variable: %v", err)
	}

	return &cfg, nil

}

func main() {
	cfg, err := InitConfig()
	if err != nil {
		log.Fatalf("Error in initconfig : %v ", err)
	}

	fmt.Println("cfg : ", cfg)
}
