package controllers

import (
	"encoding/json"
	"net/http"

	"gitlab.com/inlecom/InterQ/ledger-api-server/go-gen/models"

	"gitlab.com/inlecom/InterQ/ledger-api-server/service"
)

type ResponseFromNode struct {
	Id      string `json:"id"`
	Owner   string `json:"owner"`
	Asset   string `json:"asset"`
	Model   string `json:"model"`
	Subject string `json:"subject"`
	Timeref string `json:"ts"`
	User    string `json:"user"`
	Scope   string `json:"scope"`
	Hash    string `json:"hashval"`
	HashAlg string `json:"hashalg"`
}

type QhdApi struct {
	QhdHeader *models.QhdQhdheader
	QhdBody   map[string]interface{}
	Hash      string
}

var FabricSvc, err = service.GetFabricContract()

func respondMsg(w http.ResponseWriter, message string, httpStatusCode int) {
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(httpStatusCode)
	resp := make(map[string]string)
	resp["message"] = message
	jsonResp, _ := json.Marshal(resp)
	w.Write(jsonResp)
}
