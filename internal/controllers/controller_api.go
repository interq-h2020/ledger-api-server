package controllers

import (
	"encoding/json"
	"errors"
	"fmt"
	"log"
	"net/http"
	"regexp"
	"strings"
	"time"

	"github.com/gorilla/mux"

	"gitlab.com/inlecom/InterQ/ledger-api-server/internal/helpers"
	"gitlab.com/inlecom/InterQ/ledger-api-server/internal/tls_requests"

	"gitlab.com/inlecom/InterQ/ledger-api-server/go-gen/models"
)

	var clientCert = helpers.ReadUserCertificate()
	var clientPrivateKey = helpers.ReadUserPrivateKey()
	var serverCert = "/home/fotis/certs/data-node-certificate/interq-node-data-api-demo-node-a-dataspace-node-com-chain.pem"
	

func PublishQHD(w http.ResponseWriter, r *http.Request) error {
	requestBody := models.Body{}

	var unmarshalErr *json.UnmarshalTypeError
	decoder := json.NewDecoder(r.Body)
	decoder.DisallowUnknownFields()
	err := decoder.Decode(&requestBody)
	if err != nil {
		if errors.As(err, &unmarshalErr) {
			respondMsg(w, "Bad Request. Wrong Type Provided for field "+unmarshalErr.Field, http.StatusBadRequest)
			return nil
		} else {
			respondMsg(w, "Bad Request"+err.Error(), http.StatusBadRequest)
			return nil
		}
	}

	fmt.Println("requestBody : ", requestBody)
	fmt.Println("requestBody.Pwd : ", requestBody.Pwd)
	fmt.Println("requestBody.Cid : ", requestBody.Cid)
	fmt.Println("requestBody.Qhd.QhdHeader : ", requestBody.Qhd.QhdHeader)
	fmt.Println("requestBody.Qhd.QhdBody : ", requestBody.Qhd.QhdBody)

	//Check if pwd || cid == nil
	cid := requestBody.Cid
	pwd := requestBody.Pwd
	qhdHeader := requestBody.Qhd.QhdHeader
	qhd_Body := requestBody.Qhd.QhdBody

	if cid == "" {
		respondMsg(w, "Bad Request, missing field cid", http.StatusUnprocessableEntity)
		return nil
	}
	if pwd == "" {
		respondMsg(w, "Bad Request, missing field pwd", http.StatusUnprocessableEntity)
		return nil
	}
	if qhdHeader.Model == "" {
		respondMsg(w, "Bad Request, missing field model in qhd-header", http.StatusUnprocessableEntity)
		return nil
	}
	if qhdHeader.Owner == "" {
		respondMsg(w, "Bad Request, missing field owner in qhd-header", http.StatusUnprocessableEntity)
		return nil
	}
	if qhdHeader.Asset == "" {
		respondMsg(w, "Bad Request, missing field asset in qhd-header", http.StatusUnprocessableEntity)
		return nil
	}
	if qhdHeader.Subject == "" {
		respondMsg(w, "Bad Request, missing field subject in qhd-header", http.StatusUnprocessableEntity)
		return nil
	}
	if qhdHeader.Timeref == "" {
		respondMsg(w, "Bad Request, missing field timeref in qhd-header", http.StatusUnprocessableEntity)
		return nil
	}

	//Check that timeref is in ISO 8601-1:2019 format
	timeToBePublished, err := time.Parse(time.RFC3339, qhdHeader.Timeref)
	if err != nil {
		respondMsg(w, "Bad Request, field timeref in qhd-header is not in  ISO 8601-1:2019 format", http.StatusUnprocessableEntity)
		return nil
	}

	//Check that time is less or equal than time.Now()
	currentTime := time.Now().Format(time.RFC3339)
	currentTimeISO, _ := time.Parse(time.RFC3339, currentTime)
	if timeToBePublished.Unix() > currentTimeISO.Unix() {
		respondMsg(w, "Bad Request, field timeref in qhd-header is in future", http.StatusUnprocessableEntity)
		return nil
	}

	//Generate a unique ID uuid for the qhd
	uniqueID := helpers.GenerateUuid()
	bodyJson, err := json.Marshal(qhd_Body)
	if err != nil {
		fmt.Println("err : ", err)
	}
	fmt.Println("Timeref that got hashed : ", qhdHeader.Timeref)
	//bodyHash := helpers.GenerateHash(string(bodyJson))
	bodyHash := helpers.GenerateHash(string(bodyJson) + qhdHeader.Model + qhdHeader.Subject + qhdHeader.Owner + qhdHeader.Timeref + qhdHeader.Asset)
	fmt.Println("hash of body is : ", bodyHash)

	//Check if user's identity is valid
	
		credentialResponse, err := helpers.CheckUser(cid, pwd)
		if string(credentialResponse) == "401" {
			respondMsg(w, fmt.Sprintf("Authentication of the caller %s failed, please check again your pwd and cid fields", cid), http.StatusUnauthorized)
			return nil
		}
		if string(credentialResponse) == "502" {
			respondMsg(w, fmt.Sprintf("Service endpoint is working fine, but the IDM node is down : %s", err), http.StatusBadGateway)
			return nil
		}
	
	//Publish QHD
	scope := "myScope"
	hashAlg := "SHA256"

	//Check that there is no (timeref, model, subject) conflict in our ledger
	res, err := FabricSvc.CheckQhd(qhdHeader.Subject, qhdHeader.Model, qhdHeader.Timeref)
	if err != nil {
		respondMsg(w, fmt.Sprintf("Chaincode error %s", err.Error()), http.StatusInternalServerError)
		return nil
	}
	if string(res) == "409" {
		respondMsg(w, fmt.Sprintf("QHD natural key (model, subject, timeref) is not unique :"), http.StatusConflict)
		return nil
	}

	response, err := tls_requests.SendQhd(uniqueID, qhdHeader.Owner, qhdHeader.Asset, qhdHeader.Model, qhdHeader.Subject, qhdHeader.Timeref, string(bodyJson), clientCert, clientPrivateKey, serverCert)
	fmt.Println("i am about t send body ", string(bodyJson))
	if err != nil {
		respondMsg(w, fmt.Sprintf("The call cannot be processed due to some error condition : %s", err), http.StatusInternalServerError)
		return nil
	}
	if response != 200 {
		respondMsg(w, fmt.Sprintf("Got response : %v, from Data Node", response), http.StatusConflict)
		return nil
	}

	// add the qhd to the INTERQ DLT
	result, err := FabricSvc.PublishQhds(qhdHeader, uniqueID, bodyHash, cid, pwd, scope, hashAlg)
	if err != nil {
		respondMsg(w, fmt.Sprintf("Chaincode error %s", err.Error()), http.StatusInternalServerError)
		return nil
	}
	if string(result) == "409" {
		respondMsg(w, fmt.Sprintf("QH entity is not unique :"), http.StatusConflict)
		return nil
	}
	w.Header().Set("Content-Type", "application/json; charset=UTF-8")
	w.WriteHeader(http.StatusCreated)
	resp := make(map[string]string)
	//resp["uuid"] = string(result)
	resp["uuid"] = uniqueID
	jsonResp, _ := json.Marshal(resp)
	_, err = w.Write(jsonResp)
	if err != nil {
		respondMsg(w, fmt.Sprintf("Error in http.Writer %s", err.Error()), http.StatusInternalServerError)
		return nil
	}
	return nil
}

func SearchID(w http.ResponseWriter, r *http.Request) error {
	params := mux.Vars(r)
	id := params["id"]

	if id == "" {
		respondMsg(w, "Cannot process an empty ID ", http.StatusBadRequest)
		return nil
	}
	fmt.Println("id : ", id)
	keys, ok := r.URL.Query()["pwd"]
	if !ok || len(keys[0]) < 1 {
		log.Println("Url Param key is missing")
		respondMsg(w, "Url Param pwd is missing", http.StatusBadRequest)
		return nil
	}
	pwd := keys[0]
	fmt.Println("pwd : ", pwd)

	keys, ok = r.URL.Query()["cid"]
	if !ok || len(keys[0]) < 1 {
		log.Println("Url Param key is missing")
		respondMsg(w, "Url Param pwd is missing", http.StatusBadRequest)
		return nil
	}
	cid := keys[0]
	fmt.Println("cid : ", cid)

	// TODO commented out until tenid-service is fixed
	//Check if user's credentials are valid
	
		credentialResponse, err := helpers.CheckUser(cid, pwd)
		if string(credentialResponse) == "401" {
			respondMsg(w, fmt.Sprintf("Authentication of the caller %s failed, please check again your pwd and cid fields", cid), http.StatusUnauthorized)
			return nil
		}
		if string(credentialResponse) == "502" {
			respondMsg(w, fmt.Sprintf("Service endpoint is working fine, but the IDM node is down : %s", err), http.StatusBadGateway)
			return nil
		}
	

	result, err := FabricSvc.SearchQhdByID(id, pwd, cid)
	if err != nil {
		fmt.Println("err in Search Qhd : ", err)
		respondMsg(w, "err in SearchQhdByID : ", http.StatusInternalServerError)
	}
	if len(result) == 0 {
		respondMsg(w, fmt.Sprintf("Qhd with id: %s, does not exist on ledger", id), http.StatusNotFound)
		return nil
	} else {

		//Get qhd from data node with the specified uuid
		//qhdFromDataNode, responseStatus, err := helpers.GetID(id)
		qhdFromDataNode, err := tls_requests.GetID(id, clientCert, clientPrivateKey, serverCert)
		if err != nil {
			respondMsg(w, fmt.Sprintf("The call cannot be processed due to some error condition : %s", err), http.StatusInternalServerError)
			return nil
		}
		//if responseStatus == "200 OK" {
		if err == nil {
			fmt.Println("qhdD", qhdFromDataNode)
			fmt.Println("qhd From DataNode")
			fmt.Println("qhd From DataNode model ", qhdFromDataNode.Header.Model)
			fmt.Println("qhd From DataNode subject ", qhdFromDataNode.Header.Subject)
			fmt.Println("qhd From DataNode asset ", qhdFromDataNode.Header.Asset)
			fmt.Println("qhd From DataNode owner ", qhdFromDataNode.Header.Owner)
			fmt.Println("qhd From DataNode Timeref ", qhdFromDataNode.Header.Timeref)
			fmt.Println("qhd From DataNode body ", qhdFromDataNode.QhdBody)
			fmt.Println("qhd From DataNode uid ", qhdFromDataNode.Id)

			qhdHeader := &models.QhdQhdheader{}
			err := json.Unmarshal(result, qhdHeader)
			if err != nil {
				fmt.Println("error in unmarshalling : ", err)
				respondMsg(w, fmt.Sprintf("error in unmarshalling : %s", err), http.StatusInternalServerError)
				return nil
			}
			data := make(map[string]interface{})
			err = json.Unmarshal(result, &data)
			if err != nil {
				fmt.Println("error in unmarshalling : ", err)
				respondMsg(w, fmt.Sprintf("error in unmarshalling : %s", err), http.StatusInternalServerError)
				return nil
			}
			hash, _ := data["hashval"]
			fmt.Println("hash is : ", hash)
			//jsonBody, _ := json.Marshal(qhdFromDataNode.QhdBody)
			hashNode := helpers.GenerateHash(qhdFromDataNode.QhdBody + qhdFromDataNode.Header.Model + qhdFromDataNode.Header.Subject + qhdFromDataNode.Header.Owner + qhdFromDataNode.Header.Timeref + qhdFromDataNode.Header.Asset)
			fmt.Println("hashNode : ", hashNode)
			// i  need to do this because of json incosistency
			qhdHeader.Timeref = qhdFromDataNode.Header.Timeref
			fmt.Println("qhd got ", qhdHeader)
			fmt.Println("qhd.Owner ", qhdHeader.Owner)
			fmt.Println("qhd.Subject", qhdHeader.Subject)
			fmt.Println("qhd.Model ", qhdHeader.Model)
			fmt.Println("qhd.Asset ", qhdHeader.Asset)
			fmt.Println("qhd.Timeref ", qhdHeader.Timeref)
			if hash == hashNode {
				qhd := models.Qhd{
					QhdHeader: qhdHeader,
					QhdBody:   nil,
				}
				err := json.Unmarshal([]byte(qhdFromDataNode.QhdBody), &qhd.QhdBody)
				if err != nil {
					fmt.Println("err in unmarshal : ", err)
					return nil
				}
				w.Header().Set("Content-Type", "application/json; charset=UTF-8")
				w.WriteHeader(http.StatusOK)
				respond, _ := json.Marshal(qhd)
				_, err = w.Write(respond)
				if err != nil {
					respondMsg(w, fmt.Sprintf("Error in http.Writer %s", err.Error()), http.StatusInternalServerError)
					return nil
				}
				return nil
			} else {
				respondMsg(w, "hash received from data Node is not the same as TF's Node hash", http.StatusGone)
				return nil

			}
		} else {
			respondMsg(w, fmt.Sprintf("Service endpoint is working fine, but the Data node did not find a QHD with uuid : %s, response from Data Node : %s", id, err.Error()), http.StatusBadGateway)
			return nil
		}
	}
}
func SearchByCriteria(w http.ResponseWriter, r *http.Request) error {
	var qExists bool
	var otherMetadataExists bool
	var timeRefFrom time.Time
	var timeRefTill time.Time
	metadataMap := make(map[string]string)

	keys, ok := r.URL.Query()["pwd"]
	if !ok || len(keys[0]) < 1 {
		log.Println("Url Param key is missing")
		respondMsg(w, "Url Param pwd is missing", http.StatusBadRequest)
		return nil
	}
	pwd := keys[0]
	fmt.Println("pwd : ", pwd)

	keys, ok = r.URL.Query()["cid"]
	if !ok || len(keys[0]) < 1 {
		log.Println("Url Param key is missing")
		respondMsg(w, "Url Param pwd is missing", http.StatusBadRequest)
		return nil
	}
	cid := keys[0]
	fmt.Println("cid : ", cid)

	keys, ok = r.URL.Query()["q"]
	if !ok || len(keys[0]) < 1 {
		log.Println("Url Param key is missing")
		//respondMsg(w, "Url Param pwd is missing", http.StatusBadRequest)
		//return nil
	}
	q := ""
	if len(keys) == 1 {
		fmt.Println("len keys : ", len(keys))
		fmt.Println("len keys[0] : ", len(keys[0]))
		q = keys[0]
		qExists = true
		metadataMap["f"] = q
	}
	fmt.Println("q : ", q)

	keys, ok = r.URL.Query()["o"]
	if !ok || len(keys[0]) < 1 {
		log.Println("Url Param key is missing")
		//respondMsg(w, "Url Param pwd is missing", http.StatusBadRequest)
		//return nil
	}
	o := ""
	if len(keys) == 1 {
		o = keys[0]
		otherMetadataExists = true
		metadataMap["o"] = o
	}
	fmt.Println("owner : ", o)

	keys, ok = r.URL.Query()["a"]
	if !ok || len(keys[0]) < 1 {
		log.Println("Url Param key is missing")
		//respondMsg(w, "Url Param pwd is missing", http.StatusBadRequest)
		//return nil
	}
	a := ""
	if len(keys) == 1 {
		a = keys[0]
		otherMetadataExists = true
		metadataMap["a"] = a
	}
	fmt.Println("asset : ", a)

	keys, ok = r.URL.Query()["m"]
	if !ok || len(keys[0]) < 1 {
		log.Println("Url Param key is missing")
		//respondMsg(w, "Url Param pwd is missing", http.StatusBadRequest)
		//return nil
	}
	m := ""
	if len(keys) == 1 {
		m = keys[0]
		otherMetadataExists = true
		metadataMap["m"] = m
	}
	fmt.Println("model : ", m)

	keys, ok = r.URL.Query()["s"]
	if !ok || len(keys[0]) < 1 {
		log.Println("Url Param key is missing")
		//respondMsg(w, "Url Param pwd is missing", http.StatusBadRequest)
		//return nil
	}
	s := ""
	if len(keys) == 1 {
		s = keys[0]
		otherMetadataExists = true
		metadataMap["s"] = s
	}
	fmt.Println("subject : ", s)

	keys, ok = r.URL.Query()["tf"]
	if !ok || len(keys[0]) < 1 {
		log.Println("Url Param key is missing")
		//respondMsg(w, "Url Param pwd is missing", http.StatusBadRequest)
		//return nil
	}
	tf := ""
	if len(keys) == 1 {
		tf = keys[0]
		otherMetadataExists = true
		metadataMap["tf"] = tf

		//Check if tf is in ISO Format
		timeRefFrom, err = time.Parse(time.RFC3339, tf)
		if err != nil {
			respondMsg(w, fmt.Sprintf("Bad Request, url parameter tf : %s is not in  ISO 8601-1:2019 format", tf), http.StatusUnprocessableEntity)
			return nil
		}
	}
	fmt.Println("timeRef from  : ", tf)

	keys, ok = r.URL.Query()["tt"]
	if !ok || len(keys[0]) < 1 {
		log.Println("Url Param key is missing")
		//respondMsg(w, "Url Param pwd is missing", http.StatusBadRequest)
		//return nil
	}
	tt := ""
	if len(keys) == 1 {
		tt = keys[0]
		otherMetadataExists = true
		metadataMap["tt"] = tt

		//Check if tt is in ISO Format
		timeRefTill, err = time.Parse(time.RFC3339, tt)
		if err != nil {
			respondMsg(w, fmt.Sprintf("Bad Request, url parameter tt : %s is not in  ISO 8601-1:2019 format", tt), http.StatusUnprocessableEntity)
			return nil
		}
	}
	fmt.Println("timeRef till  : ", tt)

	if !qExists && !otherMetadataExists {
		respondMsg(w, "Neither a json notation nor metadata fields provided ", http.StatusBadRequest)
		return nil
	}
	if qExists && otherMetadataExists {
		respondMsg(w, "Provide either a json notation query or metadata fields, not both ", http.StatusBadRequest)
		return nil
	}
	if otherMetadataExists {
		//Check if tf is prior to tt if both are declared
		if tf != "" && tt != "" {
			if timeRefFrom.Unix() > timeRefTill.Unix() {
				respondMsg(w, fmt.Sprintf("tf : %s is after in time then tt %s", tf, tt), http.StatusBadRequest)
				return nil
			}
		}

		fmt.Println("Mesa sto !qExists")
		qhdsMapTimeref := make(map[string]QhdApi)
		//qhdsFromDataNode, err := helpers.GetQhdsFromJsonPath(metadataMap)
		qhdsFromDataNode, err := tls_requests.GetQHDSFromJSONPath(metadataMap, clientCert, clientPrivateKey, serverCert)
		if err != nil {
			respondMsg(w, fmt.Sprintf("The call cannot be processed due to some error condition : %s", err), http.StatusInternalServerError)
			return nil
		}
		fmt.Println("qhds from Data Node : ", qhdsFromDataNode)
		//return nil

		//check user's ID
		
			credentialResponse, err := helpers.CheckUser(cid, pwd)
			if string(credentialResponse) == "401" {
				respondMsg(w, fmt.Sprintf("Authentication of the caller %s failed, please check again your pwd and cid fields", cid), http.StatusUnauthorized)
				return nil
			}
			if string(credentialResponse) == "502" {
				respondMsg(w, fmt.Sprintf("The service endpoint is working fine but could not forward the call to the Trusted Node\n(e.g., the Trusted Node is down or unreachable) : %s", err), http.StatusBadGateway)
				return nil
			}
		

		result, err := FabricSvc.SearchByCriteria(s, m, a, o, tf, tt)
		if err != nil {
			respondMsg(w, fmt.Sprintf("Chaincode error : %s", err.Error()), http.StatusInternalServerError)
			return nil
		}
		if len(result) == 0 {
			respondMsg(w, fmt.Sprintf("Qhds with the above criteria  do not exist on ledger"), http.StatusNotFound)
			return nil
		} else {

			//var qhds []models.Qhd
			type QhdToReturn struct {
				ID      string                 `json:"qhd-id"`
				QHeader *models.QhdQhdheader   `json:"qhd-header"`
				QBody   map[string]interface{} `json:"qhd-body"`
			}
			var qhds []QhdToReturn
			var qhdsHeaders []*ResponseFromNode
			var _ = json.Unmarshal(result, &qhdsHeaders)
			fmt.Println("qhds from API ", qhdsHeaders)
			for _, qhdHeader := range qhdsHeaders {
				fmt.Println("qhdHeader Owner : ", qhdHeader.Owner)
				fmt.Println("qhdHeader Asset : ", qhdHeader.Asset)
				fmt.Println("qhdHeader Model : ", qhdHeader.Model)
				fmt.Println("qhdHeader Subject : ", qhdHeader.Subject)
				fmt.Println("qhdHeader Timeref : ", qhdHeader.Timeref)
				fmt.Println("qhdHeader Hash : ", qhdHeader.Hash)
				//timeref := qhdHeader.Timeref
				uid := qhdHeader.Id
				qhdsMapTimeref[uid] = QhdApi{
					QhdHeader: &models.QhdQhdheader{
						Owner:   qhdHeader.Owner,
						Asset:   qhdHeader.Asset,
						Model:   qhdHeader.Model,
						Subject: qhdHeader.Subject,
						Timeref: qhdHeader.Timeref,
					},
					QhdBody: nil,
					Hash:    qhdHeader.Hash,
				}

			}

			for _, qhdDataNode := range qhdsFromDataNode {
				fmt.Println("Bainw edw")
				fmt.Println("qhdDAta Node ID : ", qhdDataNode.Id)
				fmt.Println("qhdDAta Node Owner : ", qhdDataNode.Header.Owner)
				fmt.Println("qhdDAta Node Subject : ", qhdDataNode.Header.Subject)
				fmt.Println("qhdDAta Node Model : ", qhdDataNode.Header.Model)
				fmt.Println("qhdDAta Node Timeref : ", qhdDataNode.Header.Timeref)
				//timeref := qhdDataNode.Header.Timeref
				idData := qhdDataNode.Id

				//Check if timeref exists in qhdsMap
				_, qhdExists := qhdsMapTimeref[qhdDataNode.Id]
				if !qhdExists {
					continue
					//	respondMsg(w, fmt.Sprintf("The Ledger and Node Data are not synced"), http.StatusInternalServerError)
					//	return nil
				}
				fmt.Println("Den eskasa")

				//this was qhd
				//qhdTemp := models.Qhd{
				//	QhdHeader: qhdsMapTimeref[timeref].QhdHeader,
				//	QhdBody:   nil,
				//	}
				//err := json.Unmarshal([]byte(qhdDataNode.QhdBody), &qhd.QhdBody)
				//if err != nil {
				//	fmt.Println("err in unmarshal : ", err)
				//	return nil
				//	}
				qhd := QhdToReturn{
					ID:      qhdDataNode.Id,
					QHeader: qhdsMapTimeref[idData].QhdHeader,
					QBody:   nil,
				}
				fmt.Println("Ownersdsdsd : ", qhd.QHeader.Owner)
				fmt.Println("Assetsdsdsd : ", qhd.QHeader.Asset)
				fmt.Println("Timerefsdsdsd : ", qhd.QHeader.Timeref)
				err = json.Unmarshal([]byte(qhdDataNode.QhdBody), &qhd.QBody)
				if err != nil {
					fmt.Println("err in unmarshal : ", err)
					return nil
				}

				qhds = append(qhds, qhd)
				fmt.Println("hash from api : ", qhdsMapTimeref[idData].Hash)
				hashNode := helpers.GenerateHash(qhdDataNode.QhdBody + qhdDataNode.Header.Model + qhdDataNode.Header.Subject + qhdDataNode.Header.Owner + qhdDataNode.Header.Timeref + qhdDataNode.Header.Asset)
				//hashNode := helpers.GenerateHash(qhdDataNode.QhdBody)
				fmt.Println("hashNode : ", hashNode)
				if hashNode != qhdsMapTimeref[idData].Hash {
					respondMsg(w, "hash received from data Node is not the same as TF's Node hash", http.StatusGone)
					return nil
				}
			}
			w.Header().Set("Content-Type", "application/json; charset=UTF-8")
			w.WriteHeader(http.StatusOK)
			respond, _ := json.Marshal(qhds)
			_, err = w.Write(respond)
			if err != nil {
				respondMsg(w, fmt.Sprintf("Error in http.Writer %s", err.Error()), http.StatusInternalServerError)
				return nil
			}
			return nil
		}

	} else {
		fmt.Println("Bhka sto else tou qExists")

		expectedRegularExpression := regexp.MustCompile(`\$\.\[\?\(\@\.qhd-body\.[\w]* == \'[\w\W]*\'\)\]`)
		correctExpression := expectedRegularExpression.MatchString(q)
		if !correctExpression {
			respondMsg(w, fmt.Sprintf("The request payload does not meet the expectations. Bad QHD format in header : %s", "jsonpath query not as expected"), http.StatusBadRequest)
			return nil
		}
		q = strings.ReplaceAll(q, ".qhd-body", "")
		metadataMap["f"] = q

		// TODO commented out until tenid-service is fixed
		

			credentialResponse, err := helpers.CheckUser(cid, pwd)
			if string(credentialResponse) == "401" {
				respondMsg(w, fmt.Sprintf("Authentication of the caller %s failed, please check again your pwd and cid fields", cid), http.StatusUnauthorized)
				return nil
			}
			if string(credentialResponse) == "502" {
				respondMsg(w, fmt.Sprintf("The service endpoint is working fine but could not forward the call to the Trusted Node\n(e.g., the Trusted Node is down or unreachable) : %s", err), http.StatusBadGateway)
				return nil
			}
		

		//var qhdsToReturn []models.Qhd
		type QhdToReturn struct {
			ID      string                 `json:"qhd-id"`
			QHeader *models.QhdQhdheader   `json:"qhd-header"`
			QBody   map[string]interface{} `json:"qhd-body"`
		}
		var qhdsToReturn []QhdToReturn

		//qhdsFromDataNode, err := helpers.GetQhdsFromJsonPath(metadataMap)
		qhdsFromDataNode, err := tls_requests.GetQHDSFromJSONPath(metadataMap, clientCert, clientPrivateKey, serverCert)
		if err != nil {
			respondMsg(w, fmt.Sprintf("The call cannot be processed due to some error condition : %s", err), http.StatusInternalServerError)
			return nil
		}
		if len(qhdsFromDataNode) == 0 {
			respondMsg(w, fmt.Sprintf("Qhds with the above criteria  do not exist on ledger"), http.StatusNotFound)
			return nil
		}

		fmt.Println("qhds from Data Node : ", qhdsFromDataNode)
		for _, qhdFromDataNode := range qhdsFromDataNode {
			// Get qhd from api by ID
			uuid := qhdFromDataNode.Id
			result, err := FabricSvc.SearchQhdByID(uuid, pwd, cid)
			if err != nil {
				fmt.Println("err in Search Qhd : ", err)
				respondMsg(w, "err in SearchQhdByID : ", http.StatusInternalServerError)
			}
			if len(result) == 0 {
				respondMsg(w, fmt.Sprintf("Qhd with id: %s, does not exist on ledger", uuid), http.StatusNotFound)
				return nil
			}
			var qhdFromLedger *ResponseFromNode
			var _ = json.Unmarshal(result, &qhdFromLedger)

			//Check the fetched qhd hash against the hash of the qhd returned by data node
			hash := qhdFromLedger.Hash
			//hashFromDataNode := helpers.GenerateHash(qhdFromDataNode.QhdBody)
			hashFromDataNode := helpers.GenerateHash(qhdFromDataNode.QhdBody + qhdFromDataNode.Header.Model + qhdFromDataNode.Header.Subject + qhdFromDataNode.Header.Owner + qhdFromDataNode.Header.Timeref + qhdFromDataNode.Header.Asset)

			if hash == hashFromDataNode {
				//	qhd := models.Qhd{
				//		QhdHeader: &models.QhdQhdheader{
				//			Owner:   qhdFromDataNode.Header.Owner,
				//			Asset:   qhdFromDataNode.Header.Asset,
				//			Model:   qhdFromDataNode.Header.Model,
				//			Subject: qhdFromDataNode.Header.Subject,
				//			Timeref: qhdFromDataNode.Header.Timeref,
				//		},
				//		QhdBody: nil,
				//	}
				qhd := QhdToReturn{
					ID: qhdFromDataNode.Id,
					QHeader: &models.QhdQhdheader{
						Owner:   qhdFromDataNode.Header.Owner,
						Asset:   qhdFromDataNode.Header.Asset,
						Model:   qhdFromDataNode.Header.Model,
						Subject: qhdFromDataNode.Header.Subject,
						Timeref: qhdFromDataNode.Header.Timeref,
					},
					QBody: nil,
				}
				//err := json.Unmarshal([]byte(qhdFromDataNode.QhdBody), &qhd.QhdBody)
				err := json.Unmarshal([]byte(qhdFromDataNode.QhdBody), &qhd.QBody)
				if err != nil {
					fmt.Println("err in unmarshal : ", err)
					return nil
				}
				qhdsToReturn = append(qhdsToReturn, qhd)
			} else {
				respondMsg(w, "hash received from data Node is not the same as TF's Node hash", http.StatusInternalServerError)
				return nil
			}
		}
		w.Header().Set("Content-Type", "application/json; charset=UTF-8")
		w.WriteHeader(http.StatusOK)
		respond, _ := json.Marshal(qhdsToReturn)
		_, err = w.Write(respond)
		if err != nil {
			respondMsg(w, fmt.Sprintf("Error in http.Writer %s", err.Error()), http.StatusInternalServerError)
			return nil
		}
		return nil
	}
}
