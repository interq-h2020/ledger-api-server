package tls_requests

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io"
	"io/ioutil"
	"net/http"
	"net/url"
)

// returns statusCode, error
func SendQhd(uuid, owner, asset, model, subject, timeref, qhdBody, clientCert, clientPrivateKey, serverCert string) (int, error) {

	client, err := createClient4MutualTLS(clientCert, clientPrivateKey, serverCert)
	if err != nil {
		return 0, err
	}

	qhd := QHD{
		Id: uuid,
		Header: &QHDHeader{
			Owner:   owner,
			Asset:   asset,
			Model:   model,
			Subject: subject,
			Timeref: timeref,
		},
		QhdBody: qhdBody,
	}

	qhdJSON, err := json.Marshal(qhd)
	if err != nil {
		fmt.Println("error in marshalling qhd : ", err.Error())
		return 0, err
	}

	fmt.Println("qhdJSON : ", string(qhdJSON))

	postBody, err := json.Marshal(map[string]interface{}{
		"qhd": qhd,
	})
	if err != nil {
		fmt.Println("error in marshalling : ", err.Error())
		return 0, err
	}
	requestBody := bytes.NewBuffer(postBody)

	url, _ := url.Parse("https://interq-node-data-api.demo-node-a.dataspace-node.com/QualityHallmarkData")
	req := &http.Request{
		Method: "POST",
		URL:    url,
		Header: map[string][]string{
			"accept": {"text/plain"},
			"Content-Type": {"application/json"},
		},
		Body:   io.NopCloser(requestBody),
	}

	resp, err := client.Do(req)
	if err != nil {
		return 0, err
	}
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		fmt.Println("error in marshalling : ", err.Error())
		return 0, err
	}
	fmt.Println("http response status : ", resp.StatusCode)
	fmt.Println("body read : ", string(body))

	return resp.StatusCode, nil

}
