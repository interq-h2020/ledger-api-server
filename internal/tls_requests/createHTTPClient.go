package tls_requests

import (
	"crypto/tls"
	"crypto/x509"
	"io/ioutil"
	"net/http"
	"errors"
)

func createClient4MutualTLS(clientCert, clientPrivateKey, serverCert string) (*http.Client, error) {

	// Read the key pair that client will use
	// clientCert contains the path to the client's .pem file
	// clientPrivateKey contains the path to the client's .pem private key file
	cert, err := tls.LoadX509KeyPair(clientCert, clientPrivateKey)
	if err != nil {
		return &http.Client{}, err
	}

	// Create a CA certificate pool and add server's cert.pem (chain) to it
	caCert, err := ioutil.ReadFile(serverCert)
	if err != nil {
		return &http.Client{}, err
	}
	caCertPool := x509.NewCertPool()
	if !caCertPool.AppendCertsFromPEM(caCert) {
		return nil, errors.New("error appending CA cert to the Pool")
	}

	// Create the http client
	client := &http.Client{
		Transport: &http.Transport{
			TLSClientConfig: &tls.Config{
				RootCAs:      caCertPool,
				Certificates: []tls.Certificate{cert},
			},
		},
	}

	return client, nil

}
