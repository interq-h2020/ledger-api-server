package tls_requests

type Qhd struct {
	Id                string `json:"id"`
	Body              string `json:"body"`
	Owner             string `json:"owner"`
	AssetSerialNumber string `json:"assetSerialNumber"`
	AssetDescription  string `json:"assetDescription"`
	Date              string `json:"date"`
	PartSerialNumber  string `json:"partSerialNumber"`
	PartDescription   string `json:"partDescription"`
}

type QHDHeader struct {
	Owner   string `json:"owner"`
	Asset   string `json:"asset"`
	Model   string `json:"model"`
	Subject string `json:"subject"`
	Timeref string `json:"timeref"`
}

type QHD struct {
	Id      string     `json:"id"`
	Header  *QHDHeader `json:"qhd-header"`
	QhdBody string     `json:"qhd-body"`
}

type getRespBody struct {
	Qhds []QHD `json:"qhds"`
}
