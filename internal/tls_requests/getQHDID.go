package tls_requests

import (
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"net/http"
	"net/url"
)

func GetID(qhdID, clientCert, clientPrivateKey, serverCert string) (QHD, error) {
	client, err := createClient4MutualTLS(clientCert, clientPrivateKey, serverCert)
	if err != nil {
		return QHD{}, err
	}

	id := qhdID
	url, _ := url.Parse("https://interq-node-data-api.demo-node-a.dataspace-node.com/QualityHallmarkData/" + id)

	request := &http.Request{
		Method: "Get",
		URL:    url,
		Header: map[string][]string{
			"accept": {"text/plain"},
			"Content-Type": {"application/json"},
		},
	}
	responseGet, err := client.Do(request)
	if err != nil {
		return QHD{}, err
	}
	defer responseGet.Body.Close()

	body, err := ioutil.ReadAll(responseGet.Body)
	if err != nil {
		return QHD{}, err
	}

	respBody := getRespBody{}
	err = json.Unmarshal(body, &respBody)
	if err != nil {
		return QHD{}, err
	}

	fmt.Println("response from Data Node : ", respBody)
	var dataNodeResponse QHD
	if responseGet.Status == "200 OK" {
		for _, qhd := range respBody.Qhds {

			fmt.Println("http header : ", responseGet.Header)
			fmt.Println("http response status : ", responseGet.Status)
			fmt.Println("http response status 200 OK boolean test ", responseGet.Status == "200 OK")
			fmt.Println("body read : ", string(body))
			fmt.Println("owner = ", qhd.Header.Owner)
			fmt.Println("asset = ", qhd.Header.Asset)
			fmt.Println("model = ", qhd.Header.Model)
			fmt.Println("subject = ", qhd.Header.Subject)
			fmt.Println("timeref = ", qhd.Header.Timeref)
			fmt.Println("data = ", qhd.QhdBody)
			// TODO remove it only for interQ video demonstration
			if qhd.Header.Asset == "order=7653" {
				qhd.Header.Asset = "change it to produce a different hash result"
			}
			dataNodeResponse = qhd
		}
		return dataNodeResponse, nil

	} else {
		return QHD{}, errors.New(responseGet.Status)
	}

}
