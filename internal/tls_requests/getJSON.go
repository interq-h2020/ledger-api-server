package tls_requests

import (
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"net/http"
	"net/url"
)

func GetQHDSFromJSONPath(fields map[string]string, clientCert, clientPrivateKey, serverCert string) ([]QHD, error) {
	client, err := createClient4MutualTLS(clientCert, clientPrivateKey, serverCert)
	if err != nil {
		return nil, err
	}

	urlString := "https://interq-node-data-api.demo-node-a.dataspace-node.com/QualityHallmarkData"
	counter := 0

	// fields["f"] contains the json expression
	// if fields["f"] does not exist , the url must be created based on the metadata values given
	_, jsonPathExists := fields["f"]

	for metadata, value := range fields {
		if jsonPathExists {
			//
			break
		} else {
			if counter == 0 {
				if metadata == "tt" || metadata == "tf" {
					urlString = urlString + "?" + metadata + "=" + url.QueryEscape(value)
					counter++
				} else {
					urlString = urlString + "?" + metadata + "=" + value
					counter++
				}
			} else {
				if metadata == "tt" || metadata == "tf" {
					urlString = urlString + "&" + metadata + "=" + url.QueryEscape(value)
				} else {
					urlString = urlString + "&" + metadata + "=" + value
				}
			}
		}
	}

	var u *url.URL
	if jsonPathExists {
		jsonPath, exists := fields["f"]
		if !exists {
			return nil, errors.New("something went wrong and f does not exist")
		}
		u, err = url.Parse("https://interq-node-data-api.demo-node-a.dataspace-node.com/QualityHallmarkData?f=" + url.QueryEscape(jsonPath))
		if err != nil {
			return nil, err
		}
	} else {
		u, err = url.Parse(urlString)
		if err != nil {
			return nil, err
		}
	}

	fmt.Println("url String  :", urlString)
	req := &http.Request{
		Method: "GET",
		URL:    u,
		Header: map[string][]string{
			"accept": {"text/plain"},
			"Content-type": {"application/json"},
		},
	}
	fmt.Println("req : ", req.RequestURI)

	response, err := client.Do(req)
	if err != nil {
		return nil, err
	}

	bd, err := ioutil.ReadAll(response.Body)
	if err != nil {
		return nil, err
	}

	resp := getRespBody{}
	err = json.Unmarshal(bd, &resp)
	if err != nil {
		return nil, err
	}
	fmt.Println("response : ", resp)

	var qhdsFromDataNode []QHD
	for _, qhd := range resp.Qhds {
		fmt.Println("========================")
		fmt.Println("body read : ", string(bd))
		fmt.Println("owner = ", qhd.Header.Owner)
		fmt.Println("asset = ", qhd.Header.Asset)
		fmt.Println("model = ", qhd.Header.Model)
		fmt.Println("subject = ", qhd.Header.Subject)
		fmt.Println("timeref = ", qhd.Header.Timeref)
		fmt.Println("data = ", qhd.QhdBody)
		fmt.Println("========================")
		qhdsFromDataNode = append(qhdsFromDataNode, qhd)
	}
	return qhdsFromDataNode, nil

}
