package helpers

type Qhd struct {
	Id                string `json:"id"`
	Body              string `json:"body"`
	Owner             string `json:"owner"`
	AssetSerialNumber string `json:"assetSerialNumber"`
	AssetDescription  string `json:"assetDescription"`
	Date              string `json:"date"`
	PartSerialNumber  string `json:"partSerialNumber"`
	PartDescription   string `json:"partDescription"`
}
