package helpers

import (
	"bytes"
	"context"
	"encoding/json"
	"fmt"
	"io"
	"io/ioutil"
	"log"
	"net/http"
	"net/url"

	"golang.org/x/oauth2/clientcredentials"
)

type QHDHeader struct {
	Owner   string `json:"owner"`
	Asset   string `json:"asset"`
	Model   string `json:"model"`
	Subject string `json:"subject"`
	Timeref string `json:"timeref"`
}
type QHD struct {
	Id      string     `json:"id"`
	Header  *QHDHeader `json:"qhd-header"`
	QhdBody string     `json:"qhd-body"`
}

func SendQhd(uuid string, owner string, asset string, model string, subject string, timeref string, body string) (int, error) {
	fmt.Println("vim-go")

	conf := &clientcredentials.Config{
		ClientID:     "interq-node-data-api",
		ClientSecret: "1cac060a-7dc7-41f3-b3fe-6a51a8e53d87",
		Scopes:       []string{"profile", "email", "interq-node-data-api"},
		TokenURL:     "https://auth.demo.dataspace-hub.com/auth/realms/demo-node-a/protocol/openid-connect/token",
	}

	ctx := context.Background()
	token, err := conf.Token(ctx)
	if err != nil {
		log.Printf("Error : %s", err)
		return 0, err
	}
	fmt.Println("Access Token is : ", token.AccessToken)

	u, _ := url.Parse("https://interq-node-data-api.demo-node-a.dataspace-node.com/QualityHallmarkData")

	qhd := QHD{
		Id: uuid,
		Header: &QHDHeader{
			Owner:   owner,
			Asset:   asset,
			Model:   model,
			Subject: subject,
			Timeref: timeref,
		},
		QhdBody: body,
	}
	try, err := json.Marshal(qhd)
	if err != nil {
		fmt.Println("err in marshallling : ", err)
		return 0, err
	}

	fmt.Println("try : ", string(try))
	fmt.Println("qhd to be sent : ", qhd)
	postBody, err := json.Marshal(map[string]interface{}{
		"qhd": qhd,
	})
	if err != nil {
		fmt.Println("err in marshallling : ", err)
		return 0, err
	}

	fmt.Println("req body  : ", string(postBody))
	requestBody := bytes.NewBuffer(postBody)
	req := &http.Request{
		Method: "POST",
		URL:    u,
		Header: map[string][]string{
			"accept":        {"text/plain"},
			"Content-Type":  {"application/json"},
			"Authorization": {"Bearer " + token.AccessToken},
		},
		Body: io.NopCloser(requestBody),
	}

	var DefaultClient = &http.Client{}

	response, err := DefaultClient.Do(req)
	if err != nil {
		return 0, err
	}

	bd, _ := ioutil.ReadAll(response.Body)
	fmt.Print("http header : ", response.Header)
	fmt.Print("http response status : ", response.StatusCode)
	fmt.Println("body read : ", string(bd))

	return response.StatusCode, nil
}
