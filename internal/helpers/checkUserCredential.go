package helpers

import (
	"io/ioutil"
	"log"
	"net/http"
)

func CheckUser(cID string, pwd string) ([]byte, error) {
	resp, err := http.Get("http://java-server-api:9001/?cid=" + cID + "&pwd=" + pwd)
	if err != nil {
		log.Println("Error in http.Get : ", err)
		return []byte("502"), err
	}

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		log.Println("Error in reading body : ", err)
		return nil, err
	}

	//Check if wallet address is OK
	if string(body) != "OK" {
		log.Println("Wallet id not ok")
		return []byte("401"), nil
	}
	return nil, nil
}
