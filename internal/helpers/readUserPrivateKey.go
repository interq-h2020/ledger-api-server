package helpers

import (
	"io/ioutil"
	"gitlab.com/inlecom/InterQ/ledger-api-server/configs"
	"log"
	"path/filepath"
)

var cfg, err = configs.InitConfig()

func ReadUserCertificate() string {
	if err != nil {
		log.Fatal(err)
	}

	var netPath = cfg.NET_PATH
	
	certPath := filepath.Join(
		netPath,
		"organizations",
		"peerOrganizations",
		"org1.example.com",
		"users",
		"User1@org1.example.com",
		"msp",
		"signcerts",
		"cert.pem",
	)

	return certPath
}

func ReadUserPrivateKey() string {
	if err != nil {
		log.Fatal(err)
	}

	var netPath = cfg.NET_PATH
	
	credPath := filepath.Join(
		netPath,
		"organizations",
		"peerOrganizations",
		"org1.example.com",
		"users",
		"User1@org1.example.com",
		"msp",
	)
	

	keyDir := filepath.Join(credPath, "keystore")
	files, err := ioutil.ReadDir(keyDir)
	if err != nil {
		log.Fatal(err)
	}
	if len(files) != 1 {
		log.Fatalf("keystore folder should contain one file")
	}
	keyPath := filepath.Join(keyDir, files[0].Name())
	return keyPath

}
