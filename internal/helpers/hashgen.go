package helpers

import (
	"crypto/sha256"
	"fmt"
	"io"
	"log"
	"strings"
)

// Hash returns sha256 hash taken from the qhd body in a string
func GenerateHash(inputString string) string {

	input := strings.NewReader(inputString)

	hash := sha256.New()
	if _, err := io.Copy(hash, input); err != nil {
		log.Fatal(err)
	}

	sum := hash.Sum(nil)
	hashString := fmt.Sprintf("%x", sum)

	return hashString

}