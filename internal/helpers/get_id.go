package helpers

import (
	"context"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"net/url"

	"golang.org/x/oauth2/clientcredentials"
)

type ResponseBody struct {
	Qhds []QHD `json:"qhds"`
}

func GetID(id string) (QHD, string, error) {
	fmt.Println("vim-go")

	conf := &clientcredentials.Config{
		ClientID:     "interq-node-data-api",
		ClientSecret: "1cac060a-7dc7-41f3-b3fe-6a51a8e53d87",
		Scopes:       []string{"profile", "email", "interq-node-data-api"},
		TokenURL:     "https://auth.demo.dataspace-hub.com/auth/realms/demo-node-a/protocol/openid-connect/token",
	}

	ctx := context.Background()
	token, err := conf.Token(ctx)
	if err != nil {
		log.Printf("Error : %s", err)
		return QHD{}, "", err
	}
	fmt.Println("Access Token is : ", token.AccessToken)

	u, _ := url.Parse("https://interq-node-data-api.demo-node-a.dataspace-node.com/QualityHallmarkData/" + id)

	req := &http.Request{
		Method: "GET",
		URL:    u,
		Header: map[string][]string{
			"accept":        {"text/plain"},
			"Authorization": {"Bearer " + token.AccessToken},
		},
	}

	var DefaultClient = &http.Client{}

	response, err := DefaultClient.Do(req)
	if err != nil {
		return QHD{}, "", err
	}

	bd, _ := ioutil.ReadAll(response.Body)
	resp := ResponseBody{}
	err = json.Unmarshal(bd, &resp)
	if err != nil {
		return QHD{}, "", err
	}
	fmt.Println("response : ", resp)
	var qhdResponse QHD
	if response.Status == "200 OK" {
		for _, qhd := range resp.Qhds {

			fmt.Println("http header : ", response.Header)
			fmt.Println("http response status : ", response.Status)
			fmt.Println("http response status 200 OK boolean test ", response.Status == "200 OK")
			fmt.Println("body read : ", string(bd))
			fmt.Println("owner = ", qhd.Header.Owner)
			fmt.Println("asset = ", qhd.Header.Asset)
			fmt.Println("model = ", qhd.Header.Model)
			fmt.Println("subject = ", qhd.Header.Subject)
			fmt.Println("timeref = ", qhd.Header.Timeref)
			fmt.Println("data = ", qhd.QhdBody)
			qhdResponse = qhd
		}
		return qhdResponse, response.Status, nil
	} else {
		return QHD{}, response.Status, nil
	}

}
