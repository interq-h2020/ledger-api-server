package helpers

import (
	"context"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"net/url"

	"golang.org/x/oauth2/clientcredentials"
)

func GetQhdsFromJsonPath(fields map[string]string) ([]QHD, error) {

	conf := &clientcredentials.Config{
		ClientID:     "interq-node-data-api",
		ClientSecret: "1cac060a-7dc7-41f3-b3fe-6a51a8e53d87",
		Scopes:       []string{"profile", "email", "interq-node-data-api"},
		TokenURL:     "https://auth.demo.dataspace-hub.com/auth/realms/demo-node-a/protocol/openid-connect/token",
	}

	ctx := context.Background()
	token, err := conf.Token(ctx)
	if err != nil {
		log.Printf("Error : %s", err)
		return nil, err
	}
	fmt.Println("Access Token is : ", token.AccessToken)

	//u, _ := url.Parse("https://interq-node-data-api.demo-node-a.dataspace-node.com/QualityHallmarkData?c=" + url.QueryEscape(f))
	urlString := "https://interq-node-data-api.demo-node-a.dataspace-node.com/QualityHallmarkData"
	counter := 0
	_, jsonPathExists := fields["f"]
	for metadata, value := range fields {
		if jsonPathExists {
			//u, _ := url.Parse("https://interq-node-data-api.demo-node-a.dataspace-node.com/QualityHallmarkData?f=" + url.QueryEscape(jsonPath))
			break
		} else {
			if counter == 0 {
				if metadata == "tt" || metadata == "tf" {
					urlString = urlString + "?" + metadata + "=" + url.QueryEscape(value)
					counter++
				} else {
					urlString = urlString + "?" + metadata + "=" + value
					counter++
				}
			} else {
				if metadata == "tt" || metadata == "tf" {
					urlString = urlString + "&" + metadata + "=" + url.QueryEscape(value)
				} else {
					urlString = urlString + "&" + metadata + "=" + value
				}
			}
		}
	}
	var u *url.URL
	if jsonPathExists {
		jsonPath, _ := fields["f"]
		u, err = url.Parse("https://interq-node-data-api.demo-node-a.dataspace-node.com/QualityHallmarkData?f=" + url.QueryEscape(jsonPath))
		if err != nil {
			return nil, err
		}
	} else {
		u, err = url.Parse(urlString)
		if err != nil {
			return nil, err
		}
	}

	fmt.Println("url String  :", urlString)
	req := &http.Request{
		Method: "GET",
		URL:    u,
		Header: map[string][]string{
			"accept":        {"text/plain"},
			"Authorization": {"Bearer " + token.AccessToken},
		},
	}
	fmt.Println("req : ", req.RequestURI)
	var DefaultClient = &http.Client{}

	response, err := DefaultClient.Do(req)
	if err != nil {
		return nil, err
	}

	bd, err := ioutil.ReadAll(response.Body)
	if err != nil {
		return nil, err
	}
	resp := ResponseBody{}
	err = json.Unmarshal(bd, &resp)
	if err != nil {
		return nil, err
	}
	fmt.Println("response : ", resp)
	var qhdResponse []QHD
	for _, qhd := range resp.Qhds {
		fmt.Println("========================")
		//fmt.Println("http header : ", response.Header)
		//fmt.Println("http response status : ", response.Status)
		fmt.Println("body read : ", string(bd))
		fmt.Println("owner = ", qhd.Header.Owner)
		fmt.Println("asset = ", qhd.Header.Asset)
		fmt.Println("model = ", qhd.Header.Model)
		fmt.Println("subject = ", qhd.Header.Subject)
		fmt.Println("timeref = ", qhd.Header.Timeref)
		fmt.Println("data = ", qhd.QhdBody)
		fmt.Println("========================")
		qhdResponse = append(qhdResponse, qhd)
	}
	//input := strings.NewReader(fmt.Sprintf("%s", now))

	//uid, _ := uuid.NewRandomFromReader(input)
	//uID := uid.String()
	return qhdResponse, nil

}
