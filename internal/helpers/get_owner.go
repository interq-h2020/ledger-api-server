package helpers

import (
	"context"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"net/url"

	"golang.org/x/oauth2/clientcredentials"
)

func GetOwner(user string) []Qhd {

	conf := &clientcredentials.Config{
		ClientID:     "interq-node-data-api",
		ClientSecret: "1cac060a-7dc7-41f3-b3fe-6a51a8e53d87",
		Scopes:       []string{"profile", "email", "interq-node-data-api"},
		TokenURL:     "https://auth.demo.dataspace-hub.com/auth/realms/demo-node-a/protocol/openid-connect/token",
	}

	ctx := context.Background()
	token, err := conf.Token(ctx)
	if err != nil {
		log.Printf("Error : %s", err)
	}
	fmt.Println("Access Token is : ", token.AccessToken)

	u, _ := url.Parse("https://interq-node-data-api.demo-node-a.dataspace-node.com/QualityHallmarkData?u=" + user)

	req := &http.Request{
		Method: "GET",
		URL:    u,
		Header: map[string][]string{
			"accept":        {"text/plain"},
			"Authorization": {"Bearer " + token.AccessToken},
		},
	}

	var DefaultClient = &http.Client{}

	response, err := DefaultClient.Do(req)

	bd, _ := ioutil.ReadAll(response.Body)
	var qhds []Qhd
	_ = json.Unmarshal(bd, &qhds)

	//input := strings.NewReader(fmt.Sprintf("%s", now))

	//uid, _ := uuid.NewRandomFromReader(input)
	//uID := uid.String()
	return qhds
}
