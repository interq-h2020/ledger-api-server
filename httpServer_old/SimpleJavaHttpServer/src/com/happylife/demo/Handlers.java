package com.happylife.demo;

import it.eng.ten.identities.lib.config.Config;
//import	tenid-clientlib-1.0.0.it.eng.ten.identities.lib.config.Config;
//import tenid-clientlib-1.0.0.it.eng.ten.identities.lib.config.Config;
import it.eng.ten.identities.lib.utils.JsonHandler;
//import java.util.Base64;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.net.URI;
//import java.net.URL;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.cloudant.client.api.model.Response;
import com.sun.net.httpserver.Headers;
import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;

public class Handlers {
	
	

	public static Map<String, String> queryToMap(String query) {
	    if(query == null) {
	        return null;
	    }
	    Map<String, String> result = new HashMap<>();
	    for (String param : query.split("&")) {
	        String[] entry = param.split("=");
	        if (entry.length > 1) {
	            result.put(entry[0], entry[1]);
	        }else{
	            result.put(entry[0], "");
	        }
	    }
	    return result;
	}
	
	
	public static class RootHandler implements HttpHandler {

		@Override
		public  void handle(HttpExchange he) throws IOException {
			URI requestedUri = he.getRequestURI();
			
			String query = requestedUri.getRawQuery();
			Map<String, String> params = queryToMap(he.getRequestURI().getQuery());
			
			String publicKey = params.get("pub");
			System.out.println("param pubKey = " + publicKey);
	
			Config config = new Config("Org1MSP", "mychannel", "user42", "dokimh", "connection.json", "/home/fotis", "/home/fotis/go/src/github.com/ftsmchl/wallet_v2/wallet");
			System.out.println("Channel is " + config.getChannelName());
			
			it.eng.ten.identities.lib.TenIdentitiesService identitiesService = new it.eng.ten.identities.lib.TenIdentitiesServiceImpl(config);
		
			try {
			
				/*
				Base64.Decoder decoder = Base64.getDecoder();
	            KeyFactory keyFactory = KeyFactory.getInstance("EC");
	            PublicKey publicKey = keyFactory.generatePublic(new X509EncodedKeySpec(decoder.decode(pubKey)));
	            Identity identity = new Identity();
	            IdentityStatus identityStatus = new IdentityStatus();
	            identity.setIdentityStatus(identityStatus);
	            IdentityBase identityBase = new IdentityBase();
	            ECDSApublicKeyXY ecdsApublicKeyXY = ECDSA.getPublicKeyAsHex(publicKey);
	            identityBase.setpKey(JsonHandler.convertToJson(ecdsApublicKeyXY));
	            identityBase.setEnd("");
	            identityBase.setExt("ROOT");
	            identityBase.setkType("ECDSA");
	            identity.setIdentityBase(identityBase);
	            */

				
				
				
				
				
				it.eng.ten.identities.lib.model.Response response = identitiesService.submitTransaction("6vwMbLRaRoXTaw5Y7cgXsJqs3zBiYJauXjNJAFMyt2Le", "5655", "Authentication", "6vwMbLRaRoXTaw5Y7cgXsJqs3zBiYJauXjNJAFMyt2Le");
				
				if (null == response || null == response.getMessage()) {
					//throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "Response is empty");
					System.out.println("Inside if !!");
					System.out.println("Response is : " + response);
				} 
			}catch (Exception e) {				
					//log.severe(e.getMessage());
					System.out.println("Inside Exception");
					System.out.println("Error : " + e.getMessage());
					//throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "Response is empty");
				}

			
			
			String message = "<h1>Server start success if you see this message</h1>" + "<h1>Port: " +  Main.port + "</h1>";
			he.sendResponseHeaders(200, message.length());
			OutputStream os = he.getResponseBody();
			os.write(message.getBytes());
			os.close();
		}
	}

	public static class EchoHeaderHandler implements HttpHandler {

		@Override
		public void handle(HttpExchange he) throws IOException {
			Headers headers = he.getRequestHeaders();
			Set<Map.Entry<String, List<String>>> entries = headers.entrySet();
			String response = "";
			for (Map.Entry<String, List<String>> entry : entries)
				response += entry.toString() + "\n";
			he.sendResponseHeaders(200, response.length());
			OutputStream os = he.getResponseBody();
			os.write(response.toString().getBytes());
			os.close();
		}
	}

	public static class EchoGetHandler implements HttpHandler {

		@Override
		public void handle(HttpExchange he) throws IOException {
			// parse request
			Map<String, Object> parameters = new HashMap<String, Object>();
			URI requestedUri = he.getRequestURI();
			String query = requestedUri.getRawQuery();
			parseQuery(query, parameters);
			// send response
			String response = "";
			for (String key : parameters.keySet())
				response += key + " = " + parameters.get(key) + "\n";
			he.sendResponseHeaders(200, response.length());
			OutputStream os = he.getResponseBody();
			os.write(response.toString().getBytes());
			os.close();
		}

	}

	public static class EchoPostHandler implements HttpHandler {

		@Override
		public void handle(HttpExchange he) throws IOException {
			System.out.println("Served by /echoPost handler...");
			// parse request
			Map<String, Object> parameters = new HashMap<String, Object>();
			InputStreamReader isr = new InputStreamReader(he.getRequestBody(), "utf-8");
			BufferedReader br = new BufferedReader(isr);
			String query = br.readLine();
			parseQuery(query, parameters);
			// send response
			String response = "";
			for (String key : parameters.keySet())
				response += key + " = " + parameters.get(key) + "\n";
			he.sendResponseHeaders(200, response.length());
			OutputStream os = he.getResponseBody();
			os.write(response.toString().getBytes());
			os.close();

		}
	}

	@SuppressWarnings("unchecked")
	public static void parseQuery(String query, Map<String, Object> parameters) throws UnsupportedEncodingException {

		if (query != null) {
			String pairs[] = query.split("[&]");

			for (String pair : pairs) {
				String param[] = pair.split("[=]");

				String key = null;
				String value = null;
				if (param.length > 0) {
					key = URLDecoder.decode(param[0], System.getProperty("file.encoding"));
				}

				if (param.length > 1) {
					value = URLDecoder.decode(param[1], System.getProperty("file.encoding"));
				}

				if (parameters.containsKey(key)) {
					Object obj = parameters.get(key);
					if (obj instanceof List<?>) {
						List<String> values = (List<String>) obj;
						values.add(value);
					} else if (obj instanceof String) {
						List<String> values = new ArrayList<String>();
						values.add((String) obj);
						values.add(value);
						parameters.put(key, values);
					}
				} else {
					parameters.put(key, value);
				}
			}
		}
	}
}
