FROM golang:1.17.2-alpine3.13 AS build-img

RUN apk add --no-cache git

WORKDIR /tmp/InterQ

#We want to populate the module cache based on the go.{mod, sum} files.
COPY go.mod .
COPY go.sum .

RUN go mod download 

COPY . .

RUN go build -o build/main .

FROM alpine:3.13
RUN apk add ca-certificates

COPY --from=build-img /tmp/InterQ/build/main /app/main
COPY ./api/ui /app/api/ui

EXPOSE 8080

CMD ["/app/main"]
