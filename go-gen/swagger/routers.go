/*
 * InterQ Trusted Framework OpenAPI
 *
 * InterQ Trusted Framework OpenAPI
 *
 * API version: 2.0.1
 * Contact: fotis.michalopoulos@inlecomsystems.com
 */

package swagger

import (
	"fmt"
	"gitlab.com/inlecom/InterQ/ledger-api-server/configs"
	"log"
	"net/http"
	"strings"

	"github.com/gorilla/mux"
)

// Route struct
type Route struct {
	Name        string
	Method      string
	Pattern     string
	HandlerFunc http.HandlerFunc
}

// Routes Array of Route structs
type Routes []Route

// NewRouter returns a new router
func NewRouter() *mux.Router {
	envcfg, err := configs.InitConfig()
	if err != nil {
		log.Fatal(err)
	}

	router := mux.NewRouter().StrictSlash(true)
	for _, route := range routes {
		var handler http.Handler
		handler = route.HandlerFunc
		handler = Logger(handler, route.Name)

		router.
			Methods(route.Method).
			Path(route.Pattern).
			Name(route.Name).
			Handler(handler)
	}

	fs := http.FileServer(http.Dir(envcfg.WORKDIR + "/api/ui/"))
	router.PathPrefix("/api").Handler(http.StripPrefix("/api", fs))

	return router
}

// Index Index
func Index(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintf(w, "Hello World from the IterQ Trusted Framework OpenAPI !")
}

var routes = Routes{
	Route{
		"Index",
		"GET",
		"/",
		Index,
	},

	Route{
		"PublishQHD",
		strings.ToUpper("Post"),
		"/interq/tf/v1.0/qhs",
		PublishQHD,
	},

	Route{
		"SearchByCriteria",
		strings.ToUpper("Get"),
		"/interq/tf/v1.0/qhs",
		SearchByCriteria,
	},

	Route{
		"SearchID",
		strings.ToUpper("Get"),
		"/interq/tf/v1.0/qhs/{id}",
		SearchID,
	},
}
