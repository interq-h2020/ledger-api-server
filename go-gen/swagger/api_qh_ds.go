/*
 * InterQ Trusted Framework OpenAPI
 *
 * InterQ Trusted Framework OpenAPI
 *
 * API version: 2.0.1
 * Contact: fotis.michalopoulos@inlecomsystems.com
 */

package swagger

import (
	"net/http"
	"gitlab.com/inlecom/InterQ/ledger-api-server/internal/controllers"
)

// PublishQHD Handler for the PublishQHD request 
func PublishQHD(w http.ResponseWriter, r *http.Request) {
	err := controllers.PublishQHD(w, r)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)	
	}
}

// SearchByCriteria Handler for the SearchByCriteria request 
func SearchByCriteria(w http.ResponseWriter, r *http.Request) {
	err := controllers.SearchByCriteria(w, r)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)	
	}
}

// SearchID Handler for the SearchID request 
func SearchID(w http.ResponseWriter, r *http.Request) {
	err := controllers.SearchID(w, r)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)	
	}
}
