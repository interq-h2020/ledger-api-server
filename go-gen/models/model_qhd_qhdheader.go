/*
 * InterQ Trusted Framework OpenAPI
 *
 * InterQ Trusted Framework OpenAPI
 *
 * API version: 2.0.1
 * Contact: fotis.michalopoulos@inlecomsystems.com
 */

package models

type QhdQhdheader struct {
	// Link to the real-world entity. The link is expressed as a  Decentralized identity and should match the ID of the caller. 
	Owner string `json:"owner,omitempty"`
	// Name of the asset. This information is optional. Format can be freely defined by the stakeholders. 
	Asset string `json:"asset,required"`
	// Name of the assessment model the QHD conforms to. Name needs to be a globally unique identifier, it must be a URL with the following syntax : <br> interq-qhd://<domain-name>/<model-name>[/<version-number>] 
	Model string `json:"model,required"`
	// Identifier of the phenomenon under observation.  Whatever the case, it is strongly advised that subject identifiers are structured as an array of name-value pairs, with a double colon as the name-value separator and a semicolon as the array separator 
	Subject string `json:"subject,required"`
	// Point in time the assessment refers to. It’s a timestamp that follows the ISO 8601-1:2019 standard, is expressed as UTC time and has one-second resolution  
	Timeref string `json:"timeref,required"`
}
