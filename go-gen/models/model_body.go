/*
 * InterQ Trusted Framework OpenAPI
 *
 * InterQ Trusted Framework OpenAPI
 *
 * API version: 2.0.1
 * Contact: fotis.michalopoulos@inlecomsystems.com
 */

package models

type Body struct {

	Pwd string `json:"pwd,omitempty"`

	Cid string `json:"cid,omitempty"`

	Qhd *Qhd `json:"qhd,omitempty"`
}
