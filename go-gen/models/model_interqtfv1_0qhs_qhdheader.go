/*
 * InterQ Trusted Framework OpenAPI
 *
 * InterQ Trusted Framework OpenAPI
 *
 * API version: 2.0.1
 * Contact: fotis.michalopoulos@inlecomsystems.com
 */

package models

type Interqtfv10qhsQhdheader struct {

	Owner string `json:"owner,omitempty"`

	Asset string `json:"asset,omitempty"`

	Model string `json:"model,omitempty"`

	Subject string `json:"subject,omitempty"`

	Timeref string `json:"timeref,omitempty"`
}
