/*
 * InterQ Trusted Framework OpenAPI
 *
 * InterQ Trusted Framework OpenAPI
 *
 * API version: 2.0.1
 * Contact: fotis.michalopoulos@inlecomsystems.com
 */

package models

type InlineResponse200 struct {

	QhdId string `json:"qhd-id,omitempty"`

	QhdHeader *Interqtfv10qhsQhdheader `json:"qhd-header,omitempty"`

	QhdBody map[string]interface{} `json:"qhd-body,omitempty"`
}
