/*
 * InterQ Trusted Framework OpenAPI
 *
 * InterQ Trusted Framework OpenAPI
 *
 * API version: 2.0.1
 * Contact: fotis.michalopoulos@inlecomsystems.com
 */

package models

type Qhd struct {

	QhdHeader *QhdQhdheader `json:"qhd-header,required"`

	QhdBody map[string]interface{} `json:"qhd-body,required"`
}
