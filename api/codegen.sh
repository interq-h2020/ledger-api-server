curdir="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"

cd $curdir

java -cp golang-server-swagger-codegen-1.0.0.jar:swagger-codegen-cli.jar \
    io.swagger.codegen.v3.cli.SwaggerCodegen generate \
    -l golang-server \
    -i $curdir/swagger.yaml \
    -t $curdir/templates \
    -c $curdir/config.json \
    -o ../

cp $curdir/swagger.yaml $curdir/ui/
cd -

